# How to install and run the code on the local env
>
>
>Before: npm install
1. set up json server to connect json data.
> npm install -g json-server
>
2. run server first
> json-server --watch db.json --port 3001
>
3. run the project
> npm start
